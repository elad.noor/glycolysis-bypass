# The MIT License (MIT)

# Copyright (c) 2021 Weizmann Institute of Science
# Copyright (c) 2021 Elad Noor

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import argparse
import logging

import pandas as pd
from cobra.io import read_sbml_model

from equilibrator_api import Q_, ComponentContribution
from equilibrator_api.model import Bounds
from search_pathways import SearchPathways


###############################################################################

parser = argparse.ArgumentParser(description="Glycolysis bypass designer")
parser.add_argument(
    "--iter",
    type=int,
    help="the maximum number of solutions to show got each ATP yield.",
    default=5,
)
parser.add_argument(
    "--debug", action="store_true", help="flag for turning on the debug mode."
)
parser.add_argument(
    "--min_atp", type=int, help="the minimum number of ATP to consider.", default=-1
)
parser.add_argument(
    "--max_atp", type=int, help="the maximum number of ATP to consider.", default=1
)
parser.add_argument(
    "--radius",
    type=int,
    help="the radius of the elimination sphere around each of the previous solutions.",
    default=3,
)
args = parser.parse_args()
if args.debug:
    logging.getLogger().setLevel(logging.DEBUG)
else:
    logging.getLogger().setLevel(logging.INFO)

###############################################################################

cobra_model = read_sbml_model("iML1515.xml.gz")

################################################################################
# replace *redoxin (flavoredoxin and thioredoxin) with NADP(H)
thioredoxin = cobra_model.metabolites.trdrd_c
for r in thioredoxin.reactions:
    if r.get_coefficient(thioredoxin) == -1:
        r += cobra_model.reactions.TRDR
    elif r.get_coefficient(thioredoxin) == 1:
        r -= cobra_model.reactions.TRDR

flavoredoxin = cobra_model.metabolites.flxr_c
for r in flavoredoxin.reactions:
    if r.get_coefficient(flavoredoxin) == -2:
        r += cobra_model.reactions.FLDR2
    elif r.get_coefficient(flavoredoxin) == 2:
        r -= cobra_model.reactions.FLDR2


for reaction_id in [
    # remove the electron transfer reactions from NADPH to *redoxin
    "FLDR2",
    "TRDR",
    # formate-THF ligase was mistakenly included in iML1515
    "FTHFLi",
    # remove ATP wasting reactions
    "NADK",
    "ATPM",
    "ATPS4rpp",
    # remove biomass and uptake reactions
    "EX_glc__D_e",
    "BIOMASS_Ec_iML1515_WT_75p37M",
    "BIOMASS_Ec_iML1515_core_75p37M",
]:
    cobra_model.reactions.get_by_id(reaction_id).remove_from_model()

################################################################################

comp_contrib = ComponentContribution()
comp_contrib.p_h = Q_(7.5)
comp_contrib.ionic_strength = Q_("0.3 M")

CONC_BOUNDS_FNAME = "concentration_bounds.csv"
bounds_df = pd.read_csv(CONC_BOUNDS_FNAME)
bounds_df["compound"] = bounds_df["Compound:Identifiers"].apply(
    comp_contrib.get_compound
)
bounds_df = bounds_df.set_index("compound")
bounds_df["lb"] = bounds_df["Concentration:Min"].apply(Q_)
bounds_df["ub"] = bounds_df["Concentration:Max"].apply(Q_)
default_conc_lb = Q_("1e-6 M")
default_conc_ub = Q_("1e-2 M")
conc_bounds = Bounds(
    bounds_df.lb.to_dict(), bounds_df.ub.to_dict(), default_conc_lb, default_conc_ub
)
conc_bounds.check_bounds()

# make all the reactions in the model reversible, except for all reactions
# that are boundary (e.g. dummy reactions), non-cytoplasmic, or in the list
# of KOs.
for rxn in cobra_model.reactions:
    if rxn.boundary or (rxn.compartments != {"c"}):
        rxn.remove_from_model()
    else:
        rxn.bounds = (-10, 10)

###############################################################################

logging.info(
    f"Looking for pathways that convert glycerol to pyruvate "
    f"while producing different amounts of ATP - in "
    f"the genome-scale COBRA model '{cobra_model.id}'. "
    f"Stopping after {args.iter} solutions."
)

PATH_OBJECTIVE = {
    "glyc_c": -1,
    "pyr_c": 1,
}
cf = SearchPathways(cobra_model, conc_bounds, comp_contrib, maximize_mdf=False)
cf.set_pathway_objective(PATH_OBJECTIVE)
cf.add_currency_metabolites(
    ["h2o_c", "atp_c", "adp_c", "amp_c", "pi_c", "h_c", "h_e", "h_p"]
)
cf.merge_metabolites(
    "e_acceptor", ["nad_c", "nadp_c", "q8_c", "mqn8_c", "trdox_c", "2dmmq8_c"]
)
cf.merge_metabolites(
    "e_donor", ["nadh_c", "nadph_c", "q8h2_c", "mql8_c", "trdrd_c", "2dmmql8_c"]
)
cf.add_reaction(
    id="__regenerate_electron_carrier__",
    name="Regenerate Electron Carrier",
    metabolites_to_add={"e_acceptor": -1, "e_donor": 1},
    lower_bound=-1000,
    upper_bound=1000,
    objective_coefficient=0.0,
)

# add a reaction that recycles ATP, so that we iteratee
# through pathways with different ATP yields (also negative ones)
cf.add_reaction(
    id="recycle_atp",
    name="Recycle ATP",
    metabolites_to_add={"atp_c": -1, "pi_c": 1, "adp_c": 1},
    lower_bound=args.min_atp - 1e-5,
    upper_bound=args.max_atp + 1e-5,
    objective_coefficient=0.0,
)

cf.remove_metabolites(["h2o_c", "h_c", "h_e", "h_p"])
for free_met in ["pi_c", "co2_c", "o2_c", "nh4_c"]:
    cf.add_reaction(
        id=f"__regenerate_{free_met}__",
        name=f"Regenerate {free_met}",
        metabolites_to_add={free_met: 1},
        lower_bound=-1000,
        upper_bound=1000,
        objective_coefficient=0.0,
    )

cf.add_cofactors(
    [
        "adp_c",
        "atp_c",
        "e_acceptor",
        "e_donor",
        "co2_c",
        "o2_c",
        "pi_c",
        "nh4_c",
        "glu__L_c",
        "akg_c",
    ]
)

solutions = cf.search(
    f"pathways",
    max_iterations=args.iter,
    write_lp=False,
    write_full_solution=False,
    output_format="none",
    radius=args.radius,
)

logging.info(f"Found {len(solutions)} solutions, and wrote them to /pathways.tsv")
