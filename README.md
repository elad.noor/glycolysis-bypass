# glycolysis-bypass

## Awakening metabolic bypasses of glycolysis in Escherichia coli

A algorithm for finding glycolysis bypasses in *E. coli*.

Requirements:
- python 3.7+
- numpy
- scipy
- matplotlib
- pandas
- sbtab
- cobra
- optlang
- MILP solver: CPLEX or Gurobi
- sbtab
- equilibrator-api (0.4.3)

For plotting:
- escher
- pydot
- graphviz
- seaborn
- jupyterlab

## Instructions for reproducing the results

In order to reproduce the list of bypasses in our paper,
follow the instructions below. Note that the pre-computed result files
`results/pathways_*.tsv` are stored in the Git repository.

### Create a virtualenv
Check out the code, and create a virtual environment with all the requirements:
```bash
git clone https://gitlab.com/elad.noor/glycolysis-bypass.git
cd glycolysis-bypass
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Install an MILP solver
Install an MILP solver (such as CPLEX or Gurobi) in the python virtualenv,
by following the instructions provided by the vendor.
We used the Gurobi solver (version 9.1.2).

### Run the main python script
```bash
python -m search_glycolysis_bypasses
```
The results will be written in SBtab format in 3 files called:
`pathways_-1.tsv`, `pathways_0.tsv`, and `pathways_1.tsv`.


## Instructions for generating the supplementary figures

Launch Jupyter and open the notebook called `results/make_supplementary_figures.ipynb`.
Running all cells will generate the following:
- `figureS1.pdf` - the Pareto figure of all pathways
- `figureS2.pdf` - graph depictions of all pathways
- `pathway_stats.csv` - a table with general stats of all the pathways
- `pathway_summary.csv` - a table describing the pathways including MDF and the reactions used
