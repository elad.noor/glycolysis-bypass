# The MIT License (MIT)
#
# Copyright (c) 2020 Weizmann Institute of Science
# Copyright (c) 2020 Elad Noor
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
from cobra.io import read_sbml_model
import pandas as pd
from sbtab import SBtab
from path import Path


def read_result_sbtab():
    # collect the stats from the solution file
    stats_data = []
    reaction_data = []
    all_reaction_ids = set()

    for filepath in Path(".").listdir("pathways_*.tsv"):
        n_atp = int(re.findall(r"pathways_([\d\-]+)", filepath)[0])
        with open(filepath, "rt") as sbtab_file:
            document_name = "pathways"
            sbtabdoc = SBtab.SBtabDocument(document_name, sbtab_file.read(), filepath)

        for sbtab in sbtabdoc.sbtabs:
            if sbtab.table_type == "Reaction":
                i = int(re.findall(r"Pathway_(\d+)_reactions", sbtab.table_name)[0])
                for row in sbtab.to_data_frame().itertuples():
                    all_reaction_ids.add(row.Reaction)
                    flux = float(row.Value)
                    if flux > 0:
                        reaction_data.append((n_atp, i, row.Reaction + "_F", flux))
                    elif flux < 0:
                        reaction_data.append((n_atp, i, row.Reaction + "_R", -flux))

            if sbtab.table_type == "Quantity":
                i = int(re.findall(r"Pathway_(\d+)_stats", sbtab.table_name)[0])
                df = sbtab.to_data_frame().set_index("ID")
                sum_of_fluxes = float(df.at["sum_flux", "Value"])
                n_reactions = float(df.at["nr", "Value"])
                stats_data.append((n_atp, i, sum_of_fluxes, n_reactions))

    stats_df = pd.DataFrame(
        data=stats_data,
        columns=["n_atp", "pathway", "sum_of_fluxes", "n_reactions"],
    )

    reaction_df = pd.DataFrame(
        data=reaction_data,
        columns=["n_atp", "pathway", "reaction", "flux"],
    )
    return stats_df, reaction_df


COBRA_MODEL = read_sbml_model("../iML1515.xml.gz")

COFACTORS = set(
    map(
        COBRA_MODEL.metabolites.get_by_id,
        [
            "adp_c",
            "atp_c",
            "co2_c",
            "o2_c",
            "pi_c",
            "nh4_c",
            "glu__L_c",
            "akg_c",
            "h_c",
            "h_p",
            "h_e",
            "h2o_c",
            "nad_c",
            "nadh_c",
            "nadp_c",
            "nadph_c",
            "trdox_c",
            "trdrd_c",
            "q8_c",
            "q8h2_c",
            "mqn8_c",
            "mql8_c",
        ],
    )
)